
plugins {
    java
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.jetbrains.gradle.plugin.idea-ext") version "1.1.6"
}

group = "de.limited_dev"
version = "v3.2.3-b2"

repositories {
    mavenCentral()
    maven("https://m2.dv8tion.net/releases")
}

dependencies {
    implementation("net.dv8tion:JDA:5.0.0-alpha.20")
    implementation("com.sedmelluq:lavaplayer:1.3.77")
    implementation("org.slf4j:slf4j-api:2.0.3")
    implementation("org.slf4j:slf4j-simple:2.0.3")

}

val templateSource = file("src/main/templates")
val templateDest = layout.buildDirectory.dir("generated/sources/templates")

tasks {
    getByName<Test>("test") {
        useJUnitPlatform()
    }

    withType<Jar> {
        manifest {
            attributes["Main-Class"] = "de.limited_dev.botendo.Main"
        }
        // To add all of the dependencies
        from(sourceSets.main.get().output)

        dependsOn(configurations.runtimeClasspath)
        from({
            configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
        })
        dependsOn("generateTemplates", "processResources")
    }

    withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
        dependencies {
            include(dependency("net.dv8tion:JDA:5.0.0-alpha.20"))
            include(dependency("com.sedmelluq:lavaplayer:1.3.77"))
            include(dependency("org.slf4j:slf4j-api:2.0.3"))
            include(dependency("org.slf4j:slf4j-simple:2.0.3"))
        }
        dependsOn("generateTemplates", "processResources")
    }

    withType<JavaCompile>{
        options.encoding = "UTF-8"
        dependsOn("generateTemplates")
    }

    create<Copy>("generateTemplates") {
        filteringCharset = "UTF-8"
        val props = mapOf(
            "version" to version
        )
        inputs.properties(props)
        from(templateSource)
        expand(props)
        into(templateDest)
    }
}

sourceSets.main {
    java {
        srcDir(templateDest)
    }

    resources {
        srcDir("src/main/generated")
    }
}

java{
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

rootProject.idea.project {
    this as ExtensionAware
    configure<org.jetbrains.gradle.ext.ProjectSettings> {
        this as ExtensionAware
        configure<org.jetbrains.gradle.ext.TaskTriggersConfig> {
            afterSync(tasks["generateTemplates"], tasks["processResources"])
        }
    }
}
//fuck eclipse users amirite?
//rootProject.eclipse.synchronizationTasks("generateTemplates")