package de.limited_dev.botendo.jdacommands.util;

import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import de.limited_dev.botendo.util.TimeUtil;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.*;
import java.util.HashMap;

public class TimeFormatterCommand extends JDACommand {
    public TimeFormatterCommand() {
        super("timeformatter", "Get the Timestamp of provided numbers", new String[]{"long number", "etc."});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        String s = event.getOption("unixtime").getAsString();

        HashMap<String, String[]>hm = new HashMap<>();
        hm.put(s, new String[]{TimeUtil.getTimeFormatedShortend(Long.parseLong(event.getOption("unixtime").getAsString()))});

        EmbeddedMessageHelper.SendMsg(event, g.getName(), "Rechenknecht <:FeelsTastyMan:964477418809217075>", Color.RED, null, "Calculates milliseconds to days, hours, minutes and seconds.", new String[]{s}, hm, false);
    }
}
