package de.limited_dev.botendo.jdacommands.util;

import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PollCommand extends JDACommand {
    public PollCommand() {
        super("poll", "Create a poll", new String[]{"caption", "option1", "etc"});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        String caption = event.getOption("caption").getAsString();

        List<String> voteOptions = new ArrayList<>();

        String content = "";
        int index = 0;
        for (String s : optionNames) {
            OptionMapping o = event.getOption(s);
            if (o == null)
                continue;
            voteOptions.add(o.getAsString());

            content += emojiToSelection2.get(index + 1) + " - " + o.getAsString() + "\n";
            ++index;
        }

        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Poll was created", "You can view it below.");

        MessageEmbed eb = EmbeddedMessageHelper.getEmbeddedMessageAutomated(true, "Poll", caption + "\n\n" + content, null, true).build();
        event.getChannel().sendMessageEmbeds(eb).queue(message -> addReactions(message, voteOptions));
    }

    private void addReactions(Message msg, List<String> voteOption) {
        Thread thr = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < voteOption.size(); ++i) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    msg.addReaction(Emoji.fromUnicode(emojiToSelection1.get(i + 1))).queue();

                }
                Thread.currentThread().interrupt();
            }

        };
        thr.start();
    }

    private static final List<String> optionNames = List.of("option1", "option2", "option3", "option4", "option5", "option6", "option7", "option8", "option9", "option10");

    private static final HashMap<Integer, String> emojiToSelection1 = new HashMap<>() {{
        put(1, "1️⃣");
        put(2, "2️⃣");
        put(3, "3️⃣");
        put(4, "4️⃣");
        put(5, "5️⃣");
        put(6, "6️⃣");
        put(7, "7️⃣");
        put(8, "8️⃣");
        put(9, "9️⃣");
        put(10, "🔟");
    }};

    private static final HashMap<Integer, String> emojiToSelection2 = new HashMap<>() {{
        put(1, ":one:");
        put(2, ":two:");
        put(3, ":three:");
        put(4, ":four:");
        put(5, ":five:");
        put(6, ":six:");
        put(7, ":seven:");
        put(8, ":eight:");
        put(9, ":nine:");
        put(10, ":keycap_ten:");
    }};
}
