package de.limited_dev.botendo.jdacommands.util;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.build.BuildConstants;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.jdacommands.component.JDACommandManager;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import de.limited_dev.botendo.util.TimeUtil;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.*;
import java.util.HashMap;

public class InfoCommand extends JDACommand {
    public InfoCommand() {
        super("info", "Shows general infos about the bot.", null);
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        HashMap<String, String[]> hm = new HashMap<>();
        hm.put("Information", new String[]{"Version", "Ping", "Uptime", "RAM", "# of executed commands", "# Commands"});
        hm.put("Value", new String[]{BuildConstants.botVersion, Main.getJda().getGatewayPing() + "ms", TimeUtil.getTimeFormatedShortend(Main.getUptime()),
                (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024 / 1024 + "MB / " + (Runtime.getRuntime().maxMemory() - (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())) / 1024 / 1024 + "MB; " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) * 100L / Runtime.getRuntime().maxMemory() + "%",
                String.valueOf(JDACommandManager.getSlashCommandsRunCount()), JDACommandManager.getCommandsAsObjects().length + ""});
        EmbeddedMessageHelper.SendMsg(event, Main.getJda().getSelfUser().getName() + " " + BuildConstants.botVersion, "Bot information", Color.YELLOW, Main.getJda().getSelfUser().getAvatarUrl(), "General information about the bot",
                new String[]{"Information", "Value"}, hm, true);
    }
}
