package de.limited_dev.botendo.jdacommands.util;

import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.jdacommands.component.JDACommandManager;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.*;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class HelpCommand extends JDACommand {

    public HelpCommand() {
        super("help", "Lists all commands.", null);
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        HashMap<String, String[]> values = new HashMap<>();
        CopyOnWriteArrayList<String> arguments = new CopyOnWriteArrayList<>();
        CopyOnWriteArrayList<String> disc = new CopyOnWriteArrayList<>();
        for (JDACommand c : JDACommandManager.getCommandsAsObjects()) {
            String commandArgs = "";
            if (c.getCommandOptions() == null) {
                commandArgs = c.getName();
                arguments.add(commandArgs);
            } else {
                for (String ca : c.getCommandOptions()) {
                    commandArgs = commandArgs.isEmpty() ? "<" + ca + ">" : commandArgs + " <" + ca + ">";
                }
                commandArgs = c.getName() + " " + commandArgs;
                arguments.add(commandArgs);
            }

            disc.add(c.getDescription().isEmpty() ? "No description" : c.getDescription());
        }
        String[] argumentsArray = new String[arguments.size()];
        for (int i = 0; i < arguments.size(); i++) {
            argumentsArray[i] = arguments.get(i);
        }
        values.put("Command", argumentsArray);

        String[] Descriptions = new String[disc.size()];
        for (int i = 0; i < disc.size(); i++) {
            Descriptions[i] = disc.get(i);
        }
        values.put("Description", Descriptions);

        EmbeddedMessageHelper.SendMsg(event, "Uses / commands", "Commands", Color.MAGENTA,
                null, "List of avalible commands", new String[]{"Command", "Description"}, values, true, true, 0);
    }
}
