package de.limited_dev.botendo.jdacommands.util;

import de.limited_dev.botendo.features.util.PrivacyLinkFeatureDataManager;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.jdacommands.component.JDACommandValues;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class FeatureCommand extends JDACommand {
    public FeatureCommand() {
        super("feature", "Add or remove a feature", new String[]{"remove/add", "*feature*"});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        Member m = event.getMember();

        if(!m.hasPermission(Permission.ADMINISTRATOR)){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Forbidden", "You are not allowed to run this command", null);
            return;
        }

        //Add
        if(event.getOption("type").getAsString().equals("add")){
            switch(event.getOption("feature").getAsString()){
                case "privacylinkreplier":
                    JDACommandValues.DiscordServerPrivacyLink.put(g.getIdLong(), true);
                    lgr.info("Feature added: privacylinkreplier");
                    EmbeddedMessageHelper.sendSimpleOneLiner(event, "Added Privacy Link Replier", "Feature was added");
                    PrivacyLinkFeatureDataManager.getInstance().save(JDACommandValues.DiscordServerPrivacyLink);
                    break;
                default:
                    break;
            }
            return;
        //Remove
        }else if(event.getOption("type").getAsString().equals("remove")){
            switch(event.getOption("feature").getAsString()){
                case "privacylinkreplier":
                    JDACommandValues.DiscordServerPrivacyLink.put(g.getIdLong(), false);
                    lgr.info("Feature removed: privacylinkreplier");
                    EmbeddedMessageHelper.sendSimpleOneLiner(event, "Removed Privacy Link Replier", "Feature was removed");
                    PrivacyLinkFeatureDataManager.getInstance().save(JDACommandValues.DiscordServerPrivacyLink);
                    break;
                default:
                    break;
            }
        }


    }
}
