package de.limited_dev.botendo.jdacommands.util;

import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.components.buttons.Button;

import java.util.ArrayList;
import java.util.List;

public class ButtonRolesCommand extends JDACommand {
    public ButtonRolesCommand() {
        super("buttonroles", "Add Buttons to react", new String[]{"@Role", "etc."});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        //EmbeddedMessageHelper.sendSimpleOneLiner(event, "Please use " + Main.getConfigConfig().getPrefix() + "buttonroles", "Slash Commands do not work with this command yet", null);
        if (!event.getMember().hasPermission(Permission.ADMINISTRATOR)) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "You do not have the permission", "You have not the permission to use this command", null);
            return;
        }
        List<Role> rls = new ArrayList<>();
        if (event.getOption("role1") != null) {
            rls.add(event.getOption("role1").getAsRole());
        }
        if (event.getOption("role2") != null) {
            rls.add(event.getOption("role2").getAsRole());
        }
        if (event.getOption("role3") != null) {
            rls.add(event.getOption("role3").getAsRole());
        }
        if (event.getOption("role4") != null) {
            rls.add(event.getOption("role4").getAsRole());
        }
        if (event.getOption("role5") != null) {
            rls.add(event.getOption("role5").getAsRole());
        }
        if (event.getOption("role6") != null) {
            rls.add(event.getOption("role6").getAsRole());
        }
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Done", "The Message", null);
        EmbedBuilder eb = EmbeddedMessageHelper.getEmbeddedMessageAutomated(true, "Role Selector", "Get your role by clicking one of the folloing.", null, false);
        event.getChannel().asTextChannel().sendMessageEmbeds(eb.build()).setActionRow(getButtons(rls)).queue();
    }

    public List<Button> getButtons(List<Role> roles){
        List<Button> btns = new ArrayList<>();
        for(Role r : roles){
            btns.add(Button.primary(r.getId(), r.getName()));
        }
        return btns;
    }
}
