package de.limited_dev.botendo.jdacommands.util;

import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import de.limited_dev.botendo.util.TimeUtil;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.*;
import java.util.HashMap;

public class TimeUnformatterCommand extends JDACommand {
    public TimeUnformatterCommand() {
        super("timeunformatter", "Get the miliseconds of a formatted date", new String[]{"long day", "etc."});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        HashMap<String, String[]> hm = new HashMap<>();
        String in = event.getOption("timestamp").getAsString();

        hm.put(in, new String[]{String.valueOf(TimeUtil.getTimeUnformated(in))});

        EmbeddedMessageHelper.SendMsg(event, g.getName(), "Rechenknecht <:FeelsTastyMan:964477418809217075>", Color.RED, null, "Calculates days, hours, minutes and seconds to milliseconds", new String[]{in}, hm, false, true, 0);
    }
}
