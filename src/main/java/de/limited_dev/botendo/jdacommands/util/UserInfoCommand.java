package de.limited_dev.botendo.jdacommands.util;

import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class UserInfoCommand extends JDACommand {
    public UserInfoCommand() {
        super("userinfo", "Shows info about users", new String[]{"@User / UserID", "etc."});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        Member m = event.getOption("target").getAsMember();
        sendMemberinfo(event, g, m, false);
    }

    private static void sendMemberinfo(SlashCommandInteractionEvent event, Guild g, Member menMem, boolean fromMobile) {
        HashMap<String, String[]> value;
        CopyOnWriteArrayList<String> content;
        String[] contentArr;
        value = new HashMap<>();
        content = new CopyOnWriteArrayList<>();
        content.add("Top Role: ");
        content.add("Boosting: ");
        content.add("Voice: ");
        contentArr = new String[content.size()];
        for(int i = 0; i < content.size(); i++){
            contentArr[i] = content.get(i);
        }
        value.put("Information", contentArr);
        content = new CopyOnWriteArrayList<>();
        content.add(menMem.getRoles().get(0).getName());
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy, HH:mm");
        content.add(menMem.isBoosting() ? "<a:PepegaCredit:964522569350905917> " + menMem.getTimeBoosted().format(dtf) : "No");
        content.add(menMem.getVoiceState().inAudioChannel() ? menMem.getVoiceState().getChannel().getName() : "Disconnected");
        contentArr = new String[content.size()];
        for (int i = 0; i < content.size(); i++) {
            contentArr[i] = content.get(i);
        }
        value.put("Value", contentArr);

        EmbeddedMessageHelper.SendMsg(event, g.getName(), menMem.getUser().getName() + (menMem.getNickname() != null ? " // " + menMem.getNickname() : "") + (menMem.getUser().isBot() ? " <a:COGGERS:964520673257066506>" : ""), Color.RED, menMem.getUser().getAvatarUrl(), "User information", new String[]{"Information", "Value"}, value, true, fromMobile, 0);
    }
}
