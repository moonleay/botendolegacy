package de.limited_dev.botendo.jdacommands.music.playlists;

import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.utils.FileUpload;

import java.io.File;
import java.util.Objects;

public class ExportPlaylistCommand extends JDACommand {
    public ExportPlaylistCommand() {
        super("exportplaylist", "Export a Playlist", new String[]{"playlist name"});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        Member m = event.getMember();
        Playlist pl = PlaylistManager.getPlaylistFromName(g, event.getOption("playlistname").getAsString());
        if (pl == null) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "This playlist does not exist");
            return;
        }
        if (!pl.isPublic() && !Objects.equals(pl.getOwnerID(), m.getUser().getIdLong())) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "You don't own this playlist.");
            return;
        }
        File db = new File("./data/playlists/" + g.getIdLong() + "/" + pl.getPlaylistName() + "/playlist.list");
        if (!db.exists()) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "We could not find the file you were looking for.");
            return;
        }
        EmbedBuilder eb = EmbeddedMessageHelper.getEmbeddedMessageAutomated(true, pl.getPlaylistName() + " playlist", "Above is the data for the playlist.", null, true);

        m.getUser().openPrivateChannel().submit()
                .thenCompose(channel -> channel.sendMessageEmbeds(eb.build()).addFiles(FileUpload.fromData(db)).submit())
                .whenComplete((message, error) -> {
                    if (error != null)
                        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "The playlist could not be send to you, because you have PMs disabled.\nEnable PMs and try again.");
                    else
                        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Success", "Sent playlist data to you in your DMs.");
                });

    }
}
