package de.limited_dev.botendo.jdacommands.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import de.limited_dev.botendo.features.music.GuildMusicManager;
import de.limited_dev.botendo.features.music.MusicManager;
import de.limited_dev.botendo.features.music.component.MemeSupplier;
import de.limited_dev.botendo.features.music.component.MemeType;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import de.limited_dev.botendo.util.TimeUtil;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class QueueCommand extends JDACommand {
    public QueueCommand() {
        super("queue", "Show the music queue", null);
    }


    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        final Guild g = event.getGuild();
        final GuildMusicManager guildMusicManager = MusicManager.getInstance().getGuildMusicManager(g);
        final BlockingQueue<AudioTrack> queue = guildMusicManager.scheduler.queue;
        final AudioPlayer audioPlayer = guildMusicManager.audioPlayer;
        final AudioTrack currenttrack = audioPlayer.getPlayingTrack();

        if(queue.isEmpty() && currenttrack == null){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Queue", "**Queue Empty**", MemeSupplier.getMemeLink(MemeType.EMPTY_QUEUE));
            return;
        }

        final List<AudioTrack> trackList = new ArrayList<>(queue);
        String description = "";
        AudioTrackInfo info;
        String trackInfo;
        int index = 0;
        info = currenttrack.getInfo();
        description = "**" + info.title + " - " + TimeUtil.getTimeFormatedShortend(info.length) + " (" + info.author + ")**\n";
        for(AudioTrack track : trackList){
            if(index == 14)
                continue;
            info = track.getInfo();
            trackInfo = info.title + " - " + TimeUtil.getTimeFormatedShortend(track.getDuration()) + " (" + info.author + ")\n";
            description = description + trackInfo;
            ++index;
        }

        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Queue", description, MemeSupplier.getMemeLink(MemeType.QUEUE));
    }
}
