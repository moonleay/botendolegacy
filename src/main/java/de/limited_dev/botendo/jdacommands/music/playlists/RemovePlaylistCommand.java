package de.limited_dev.botendo.jdacommands.music.playlists;

import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.util.Objects;

public class RemovePlaylistCommand extends JDACommand {
    public RemovePlaylistCommand() {
        super("removeplaylist", "Remove a Playlist", new String[]{"Playlist name"});
    }


    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        Member m = event.getMember();
        String name = event.getOption("playlistname").getAsString();
        Playlist pl = PlaylistManager.getPlaylistFromName(g, name);

        if (pl == null) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Playlist not found", "This Playlist does not exist.", null);
            return;
        }

        if (!Objects.equals(pl.getOwnerID(), m.getUser().getIdLong())) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "You don't own this playlist.");
            return;
        }

        if (PlaylistManager.removePlaylist(pl, m.getUser())) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Removed Playlist " + name, "The Playlist was removed", null);
            return;
        }
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Couldn't remove playlist", "The playlist was not removed.", null);
    }
}
