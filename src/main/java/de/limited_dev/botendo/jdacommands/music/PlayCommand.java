package de.limited_dev.botendo.jdacommands.music;

import de.limited_dev.botendo.features.music.MusicManager;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.managers.AudioManager;

public class PlayCommand extends JDACommand {
    public PlayCommand() {
        super("play", "Play music with the bot", new String[]{ "YouTubeLink / Search Query" });
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        final Guild g = event.getGuild();
        final Member self = g.getSelfMember();
        final Member m = g.getMemberById(event.getUser().getIdLong());
        final GuildVoiceState selfState = self.getVoiceState();
        final GuildVoiceState memberState = m.getVoiceState();


        boolean hadToJoin = false;
        if(!selfState.inAudioChannel()){
            if(!attemptToJoinVoice(event, m))
                return;
            hadToJoin = true;
        }

        if(!hadToJoin){
            if(!selfState.getChannel().equals(memberState.getChannel())){
                EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "You are not in the same voice channel", null);
                return;
            }
        }

        String link = event.getOption("linkquery").getAsString();

        if(!link.startsWith("https://www.youtube.com") && !link.startsWith("https://youtube.com") && link.startsWith("https://")){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "Not a YouTube URL.", null);
            return;
        }

        if(!isURL(link)){
            link = "ytsearch:" + link;
            lgr.info("Is not link");
        }


        MusicManager.getInstance().loadAndPlay(event, link);
    }

    private boolean isURL(String uri){
        return uri.startsWith("https://www.youtube.com") || uri.startsWith("https://youtube.com") || uri.startsWith("https://youtu.be");
    }

    public boolean attemptToJoinVoice(SlashCommandInteractionEvent event, Member m){
        Guild g = event.getGuild();
        GuildVoiceState memberVoiceState = m.getVoiceState();
        if(!memberVoiceState.inAudioChannel()){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not In Voice",
                    "You are not in a voice channel.", null);
            lgr.info("Could not join vc");
            return false;
        }
        final AudioManager audioManager = g.getAudioManager();
        final VoiceChannel vc = memberVoiceState.getChannel().asVoiceChannel();

        audioManager.openAudioConnection(vc);
        lgr.info("Joined VC in " + g.getName());
        return true;
        //SendEmbeddedMessageHelper.sendSimpleOneLiner(g, c.getIdLong(), "", "", null);
    }
}
