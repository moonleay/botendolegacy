package de.limited_dev.botendo.jdacommands.music.playlists;

import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import de.limited_dev.botendo.features.music.PlaylistDataManager;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class ImportPlaylistCommand extends JDACommand {
    public ImportPlaylistCommand() {
        super("importplaylist", "description", new String[]{"Playlist File", "Name", "Is Public?"});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        Member m = event.getMember();
        String name = event.getOption("playlistname").getAsString();
        File destFldr = new File("./data/dwnlds/");
        if (!destFldr.exists()) {
            destFldr.mkdirs();
        }
        boolean isPublic = event.getOption("public").getAsBoolean();
        Message.Attachment attachment = event.getOption("file").getAsAttachment();
        lgr.info(attachment.getFileName());

        Playlist pl = new Playlist(g.getIdLong(), m.getIdLong(), name, isPublic);
        if (PlaylistManager.addPlaylist(pl)) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Added Playlist " + name, "This playlist is " + (isPublic ? "Public" : "Private"), null);
            long time = System.currentTimeMillis();
            File dest = new File("./data/dwnlds/" + m.getUser().getIdLong() + "." + time);
            if (dest.exists())
                dest.delete();
            try {
                dest.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            CompletableFuture<File> future = attachment.getProxy().downloadToFile(dest);
            future.exceptionally(error -> { // handle possible errors
                error.printStackTrace();
                return null;
            });
            System.out.print("Downloading");
            while (!future.isDone()) {
                //wait
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.print(".");
            }
            System.out.print("\n");
            try {
                FileReader reader = new FileReader(dest.getAbsolutePath());
                BufferedReader bufferedReader = new BufferedReader(reader);

                String line;

                while ((line = bufferedReader.readLine()) != null && line != "") {
                    pl.addSong(line);
                }

                bufferedReader.close();
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            PlaylistDataManager.getInstance().savePlaylist(pl);
            dest.delete();
            lgr.info("Imported " + name + " in " + g.getName());
            return;
        }
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "This playlist name is taken", "This playlist's name is taken", null);
    }
}
