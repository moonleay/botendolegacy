package de.limited_dev.botendo.jdacommands.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import de.limited_dev.botendo.features.music.GuildMusicManager;
import de.limited_dev.botendo.features.music.MusicManager;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import de.limited_dev.botendo.util.TimeUtil;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class NowPlayingCommand extends JDACommand {
    public NowPlayingCommand() {
        super("nowplaying", "Show what's currently playing", null);
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        final TextChannel c = event.getChannel().asTextChannel();
        final Guild g = event.getGuild();
        final Member self = g.getSelfMember();
        final GuildVoiceState selfState = self.getVoiceState();

        if(!selfState.inAudioChannel()){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not in VC", "I'm not playing anything.", null);
            return;
        }

        final GuildMusicManager guildMusicManager = MusicManager.getInstance().getGuildMusicManager(g);
        final AudioPlayer audioPlayer = guildMusicManager.audioPlayer;
        final AudioTrack track = audioPlayer.getPlayingTrack();

        if(track == null){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not Playing", "Not playing anything", null);
            return;
        }

        final AudioTrackInfo info = track.getInfo();
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Currently playing",
                "**" + info.title + "**\n" +
                        "by " + info.author + "\n" +
                        TimeUtil.getTimeFormatedShortend(audioPlayer.getPlayingTrack().getPosition()) + " : " + TimeUtil.getTimeFormatedShortend(info.length) + "\n\n" +
                        ">>>" + info.uri, "https://img.youtube.com/vi/" + getImgURL(info.uri) + "/maxresdefault.jpg");
    }

    private String getImgURL(String uri){
        return uri.split("=")[1];
    }
}
