package de.limited_dev.botendo.jdacommands.music.playlists;

import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class AddPlaylistCommand extends JDACommand {
    public AddPlaylistCommand() {
        super("addplaylist", "Add a Playlist", new String[]{"Playlist name"});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        Member m = event.getMember();
        String name = event.getOption("playlistname").getAsString();
        boolean isPublic = event.getOption("public").getAsBoolean();
        Playlist pl = new Playlist(g.getIdLong(), m.getIdLong(), name, isPublic);

        if (PlaylistManager.addPlaylist(pl)) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Added Playlist " + name, "This playlist is " + (isPublic ? "Public" : "Private"), null);
            return;
        }
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "This playlist name is taken", "This playlist's name is taken", null);
    }
}
