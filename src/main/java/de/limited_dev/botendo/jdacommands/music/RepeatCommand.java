package de.limited_dev.botendo.jdacommands.music;

import de.limited_dev.botendo.features.music.GuildMusicManager;
import de.limited_dev.botendo.features.music.MusicManager;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class RepeatCommand extends JDACommand {
    public RepeatCommand() {
        super("repeat", "Repeat the current song");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        final Guild g = event.getGuild();
        final Member self = g.getSelfMember();
        final GuildVoiceState selfState = self.getVoiceState();

        if(!selfState.inAudioChannel()){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not In VC", "I'm not a voice channel");
            return;
        }

        final Member m = g.getMemberById(event.getUser().getIdLong());
        final GuildVoiceState mState = m.getVoiceState();

        if(!mState.inAudioChannel()){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not in VC", "You are not in a vc");
            return;
        }

        if(!selfState.getChannel().asVoiceChannel().equals(mState.getChannel().asVoiceChannel())){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not in same VC", "You are not in my vc");
            return;
        }

        final GuildMusicManager guildMusicManager = MusicManager.getInstance().getGuildMusicManager(g);
        final boolean nowRepeating = !guildMusicManager.scheduler.repeating;

        guildMusicManager.scheduler.repeating = nowRepeating;

        EmbeddedMessageHelper.sendSimpleOneLiner(event, nowRepeating ? "Now Repeating" : "Now Continuing", nowRepeating ? "The current track will now loop" : "The next track will play when this song finishes");
    }
}
