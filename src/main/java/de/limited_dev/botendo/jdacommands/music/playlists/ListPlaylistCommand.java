package de.limited_dev.botendo.jdacommands.music.playlists;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ListPlaylistCommand extends JDACommand {
    public ListPlaylistCommand() {
        super("listplaylists", "List all Playlists", null);
    }


    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        Member m = event.getMember();
        List<Playlist> playlists = PlaylistManager.getListOfPlaylistsForGuild(g);
        HashMap<String, String[]> values = new HashMap<>();
        CopyOnWriteArrayList<String> names = new CopyOnWriteArrayList<>();
        CopyOnWriteArrayList<String> owners = new CopyOnWriteArrayList<>();
        CopyOnWriteArrayList<String> sizes = new CopyOnWriteArrayList<>();
        CopyOnWriteArrayList<String> privates = new CopyOnWriteArrayList<>();
        for (Playlist pl : playlists) {
            names.add(pl.getPlaylistName());
            owners.add(g.getMemberById(pl.getOwnerID()).getAsMention());
            sizes.add(pl.getLinkList().size() + "");
            privates.add(pl.isPublic() ? "Yes" : "No");
        }
        String[] namesArr = new String[names.size()];
        for (int i = 0; i < names.size(); i++) {
            namesArr[i] = names.get(i);
        }
        values.put("Name", namesArr);

        String[] ownersArr = new String[owners.size()];
        for (int i = 0; i < owners.size(); i++) {
            ownersArr[i] = owners.get(i);
        }
        values.put("Owner", ownersArr);

        String[] sizesArr = new String[sizes.size()];
        for (int i = 0; i < sizes.size(); i++) {
            sizesArr[i] = sizes.get(i);
        }
        values.put("# of Links", sizesArr);

        String[] privatesArr = new String[privates.size()];
        for (int i = 0; i < privates.size(); i++) {
            privatesArr[i] = privates.get(i);
        }
        values.put("Is Public?", privatesArr);
        EmbeddedMessageHelper.SendMsg(event, Main.getJda().getSelfUser().getName(), "Playlists", Color.MAGENTA,
                null, "List of playlists from this server", new String[]{"Name", "Owner", "# of Links"}, values, true);
    }
}
