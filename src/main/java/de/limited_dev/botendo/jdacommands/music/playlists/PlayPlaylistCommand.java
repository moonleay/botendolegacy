package de.limited_dev.botendo.jdacommands.music.playlists;

import de.limited_dev.botendo.features.music.MusicManager;
import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.managers.AudioManager;

import java.util.Objects;

public class PlayPlaylistCommand extends JDACommand {
    public PlayPlaylistCommand() {
        super("playplaylist", "Play a playlist", new String[]{"Playlistname"});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        final Guild g = event.getGuild();
        final Member self = g.getSelfMember();
        final Member m = g.getMemberById(event.getUser().getIdLong());
        final GuildVoiceState selfState = self.getVoiceState();
        final GuildVoiceState memberState = m.getVoiceState();


        boolean hadToJoin = false;
        if (!selfState.inAudioChannel()) {
            if (!attemptToJoinVoice(event, m))
                return;
            hadToJoin = true;
        }

        if (!hadToJoin) {
            if (!selfState.getChannel().equals(memberState.getChannel())) {
                EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "You are not in the same voice channel", null);
                return;
            }
        }

        Playlist pl = PlaylistManager.getPlaylistFromName(g, event.getOption("playlistname").getAsString());

        if (!pl.isPublic() && !Objects.equals(pl.getOwnerID(), m.getUser().getIdLong())) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "You don't own this playlist.");
            return;
        }

        for (String s : pl.getLinkList()) {
            MusicManager.getInstance().loadAndPlay(event, s, true);
        }
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Now Playing", "The \"" + pl.getPlaylistName() + "\" list!");
    }

    public boolean attemptToJoinVoice(SlashCommandInteractionEvent event, Member m) {
        Guild g = event.getGuild();
        GuildVoiceState memberVoiceState = m.getVoiceState();
        if (!memberVoiceState.inAudioChannel()) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not In Voice",
                    "You are not in a voice channel.", null);
            lgr.info("Could not join vc");
            return false;
        }
        final AudioManager audioManager = g.getAudioManager();
        final VoiceChannel vc = memberVoiceState.getChannel().asVoiceChannel();

        audioManager.openAudioConnection(vc);
        lgr.info("Joined VC in " + g.getName());
        return true;
        //SendEmbeddedMessageHelper.sendSimpleOneLiner(g, c.getIdLong(), "", "", null);
    }
}
