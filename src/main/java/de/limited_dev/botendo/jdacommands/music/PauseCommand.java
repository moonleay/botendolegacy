package de.limited_dev.botendo.jdacommands.music;

import de.limited_dev.botendo.features.music.GuildMusicManager;
import de.limited_dev.botendo.features.music.MusicManager;
import de.limited_dev.botendo.features.music.component.MemeSupplier;
import de.limited_dev.botendo.features.music.component.MemeType;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.managers.AudioManager;

public class PauseCommand extends JDACommand {
    public PauseCommand() {
        super("pause", "Pause the current song");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        final Guild g = event.getGuild();
        final Member self = g.getSelfMember();
        final GuildVoiceState selfState = self.getVoiceState();

        if(!selfState.inAudioChannel()){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not In VC", "I'm not a voice channel", null);
            return;
        }

        final Member m = g.getMemberById(event.getUser().getIdLong());
        final GuildVoiceState mState = m.getVoiceState();

        if(!mState.inAudioChannel()){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not in VC", "You are not in a vc", null);
            return;
        }

        if(!selfState.getChannel().asVoiceChannel().equals(mState.getChannel().asVoiceChannel())){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not in same VC", "You are not in my vc", null);
            return;
        }

        final GuildMusicManager guildMusicManager = MusicManager.getInstance().getGuildMusicManager(g);

        if(guildMusicManager.scheduler.player.isPaused()){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Already paused", "The Song is already paused.");
            return;
        }

        guildMusicManager.scheduler.player.setPaused(true);
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "I paused", "I paused the song", MemeSupplier.getMemeLink(MemeType.STOP));
    }
}
