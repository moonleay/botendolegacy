package de.limited_dev.botendo.jdacommands.music.playlists;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.*;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

public class ListAllLinksInPlaylistCommand extends JDACommand {
    public ListAllLinksInPlaylistCommand() {
        super("listallinplaylist", "List all links in a playlist", new String[]{"Playlist name"});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        Member m = event.getMember();
        Playlist pl = PlaylistManager.getPlaylistFromName(g, event.getOption("playlistname").getAsString());
        if (pl == null) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "This playlist does not exist");
            return;
        }
        if (!pl.isPublic() && !Objects.equals(pl.getOwnerID(), m.getUser().getIdLong())) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "You don't own this playlist.");
            return;
        }
        HashMap<String, String[]> values = new HashMap<>();
        CopyOnWriteArrayList<String> indexs = new CopyOnWriteArrayList<>();
        CopyOnWriteArrayList<String> links = new CopyOnWriteArrayList<>();
        int index = 1;
        for (String s : pl.getLinkList()) {
            indexs.add(index + "");
            links.add(s);
            ++index;
        }
        String[] IndexArr = new String[indexs.size()];
        for (int i = 0; i < indexs.size(); i++) {
            IndexArr[i] = indexs.get(i);
        }
        values.put("Index", IndexArr);

        String[] linksArr = new String[links.size()];
        for (int i = 0; i < links.size(); i++) {
            linksArr[i] = links.get(i);
        }
        values.put("Link", linksArr);
        EmbeddedMessageHelper.SendMsg(event, Main.getJda().getSelfUser().getName(), "Playlist \"" + pl.getPlaylistName() + "\"", Color.MAGENTA,
                null, "List of Links saved in playlist " + pl.getPlaylistName(), new String[]{"Index", "Link"}, values, true);
    }
}
