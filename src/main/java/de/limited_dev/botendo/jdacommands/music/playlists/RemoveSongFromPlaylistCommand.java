package de.limited_dev.botendo.jdacommands.music.playlists;

import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import de.limited_dev.botendo.features.music.PlaylistDataManager;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.util.Objects;

public class RemoveSongFromPlaylistCommand extends JDACommand {
    public RemoveSongFromPlaylistCommand() {
        super("removesongfromplaylist", "Remove a Song from a playlist", new String[]{"Playlist name", "index"});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        final Guild g = event.getGuild();
        final Member m = event.getMember();
        String name = event.getOption("playlistname").getAsString();
        int index = event.getOption("index").getAsInt();


        Playlist pl = PlaylistManager.getPlaylistFromName(g, name);
        if (pl == null) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "This playlist does not exist");
            return;
        }
        if (!pl.isPublic() && !Objects.equals(pl.getOwnerID(), m.getUser().getIdLong())) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "You don't own this playlist.");
            return;
        }
        if (index > pl.getLinkList().size()) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "This entry does not exist.");
            return;
        }
        pl.removeSongWithIndex(index - 1);
        PlaylistDataManager.getInstance().savePlaylist(pl);
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Link has been removed", "You removed index " + index + " from " + name);
    }
}
