package de.limited_dev.botendo.jdacommands.music.playlists;

import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import de.limited_dev.botendo.features.music.PlaylistDataManager;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.util.Objects;

public class AddSongToPlaylistCommand extends JDACommand {
    public AddSongToPlaylistCommand() {
        super("addsongtoplaylist", "Add a Song to a playlist", new String[]{"Playlist name", "YouTube-Link"});
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        Member m = event.getMember();
        String name = event.getOption("playlistname").getAsString();
        String link = event.getOption("youtubelink").getAsString();

        if (!link.startsWith("https://www.youtube.com") && !link.startsWith("https://youtube.com") && link.startsWith("https://")) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "Not a YouTube URL.");
            return;
        }

        if (!isURL(link)) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "Not a Youtube-link");
            return;
        }

        Playlist pl = PlaylistManager.getPlaylistFromName(g, name);
        if (pl == null) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "This playlist does not exist");
            return;
        }
        if (!pl.isPublic() && !Objects.equals(pl.getOwnerID(), m.getUser().getIdLong())) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "You don't own this playlist.");
            return;
        }
        pl.addSong(link);
        PlaylistDataManager.getInstance().savePlaylist(pl);
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Link has been added", "You added " + link + " to " + name);
    }

    private boolean isURL(String uri) {
        return uri.startsWith("https://www.youtube.com") || uri.startsWith("https://youtube.com") || uri.startsWith("https://youtu.be");
    }
}
