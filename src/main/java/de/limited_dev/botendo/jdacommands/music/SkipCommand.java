package de.limited_dev.botendo.jdacommands.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import de.limited_dev.botendo.features.music.GuildMusicManager;
import de.limited_dev.botendo.features.music.MusicManager;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class SkipCommand extends JDACommand {
    public SkipCommand() {
        super("skip", "Skip the current song.", null);
    }


    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        final Guild g = event.getGuild();
        final Member self = g.getSelfMember();
        final GuildVoiceState selfState = self.getVoiceState();

        if(!selfState.inAudioChannel()){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not In VC", "I'm not a voice channel", null);
            return;
        }

        final Member m = g.getMemberById(event.getUser().getIdLong());
        final GuildVoiceState mState = m.getVoiceState();

        if(!mState.inAudioChannel()){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not in VC", "You are not in a vc", null);
            return;
        }

        if(!selfState.getChannel().asVoiceChannel().equals(mState.getChannel().asVoiceChannel())){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Not in same VC", "You are not in my vc", null);
            return;
        }

        final GuildMusicManager guildMusicManager = MusicManager.getInstance().getGuildMusicManager(g);
        final AudioPlayer audioPlayer = guildMusicManager.audioPlayer;

        if(audioPlayer.getPlayingTrack() == null){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Error", "There is no playing track currently", null);
            return;
        }

        guildMusicManager.scheduler.nextTrack();
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Skip", "Song Skipped", "https://cdn.discordapp.com/attachments/833442323160891452/1031670974568988783/unknown.png");
    }
}
