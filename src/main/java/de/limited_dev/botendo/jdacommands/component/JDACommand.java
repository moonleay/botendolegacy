package de.limited_dev.botendo.jdacommands.component;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.util.Logger;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import javax.annotation.Nullable;

public class JDACommand {

    protected Logger lgr = Main.getLgr();
    protected JDA jda = Main.getJda();
    private final String name;
    private final String description;
    private String[] options;

    public JDACommand(String name, String description) {
        this(name, description, null);
    }

    public JDACommand(String name, String description, @Nullable String[] commandOptions) {
        this.name = name;
        this.description = description;
        if (commandOptions == null) {
            return;
        }
        this.options = commandOptions;
    }

    public void onSlashCommand(SlashCommandInteractionEvent event){

    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String[] getCommandOptions() {
        return options;
    }
}
