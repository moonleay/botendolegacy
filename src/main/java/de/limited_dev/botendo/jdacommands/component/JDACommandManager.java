package de.limited_dev.botendo.jdacommands.component;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.features.util.PrivacyLinkFeatureDataManager;
import de.limited_dev.botendo.jdacommands.music.*;
import de.limited_dev.botendo.jdacommands.music.playlists.*;
import de.limited_dev.botendo.jdacommands.util.*;
import de.limited_dev.botendo.util.Logger;
import net.dv8tion.jda.api.entities.Guild;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class JDACommandManager {

    private static final Logger lgr = Main.getLgr();

    private static final CopyOnWriteArrayList<JDACommand> JDA_COMMANDS = new CopyOnWriteArrayList<>();

    private static long slashCommandsRunCount = 0;
    private static long lastCommandChannelID = 0L;
    private static long lastGuildID = 0L;

    public static JDACommand[] getCommandsAsObjects() {
        JDACommand[] commands_ = new JDACommand[JDA_COMMANDS.size()];
        for (int i = 0; i < JDA_COMMANDS.size(); i++) {
            commands_[i] = JDA_COMMANDS.get(i);
        }
        return commands_;
    }

    public static void registerCommands() {
        JDA_COMMANDS.add(new InfoCommand());
        JDA_COMMANDS.add(new HelpCommand());
        JDA_COMMANDS.add(new UserInfoCommand());
        JDA_COMMANDS.add(new TimeFormatterCommand());
        JDA_COMMANDS.add(new TimeUnformatterCommand());
        JDA_COMMANDS.add(new ButtonRolesCommand());
        JDA_COMMANDS.add(new PlayCommand());
        JDA_COMMANDS.add(new StopCommand());
        JDA_COMMANDS.add(new NowPlayingCommand());
        JDA_COMMANDS.add(new QueueCommand());
        JDA_COMMANDS.add(new SkipCommand());
        JDA_COMMANDS.add(new PauseCommand());
        JDA_COMMANDS.add(new ContinueCommand());
        JDA_COMMANDS.add(new RepeatCommand());
        JDA_COMMANDS.add(new AddPlaylistCommand());
        JDA_COMMANDS.add(new ListPlaylistCommand());
        JDA_COMMANDS.add(new RemovePlaylistCommand());
        JDA_COMMANDS.add(new AddSongToPlaylistCommand());
        JDA_COMMANDS.add(new ListAllLinksInPlaylistCommand());
        JDA_COMMANDS.add(new RemoveSongFromPlaylistCommand());
        JDA_COMMANDS.add(new PlayPlaylistCommand());
        JDA_COMMANDS.add(new ExportPlaylistCommand());
        JDA_COMMANDS.add(new ImportPlaylistCommand());
        JDA_COMMANDS.add(new FeatureCommand());

        lgr.info("All Commands registered");

        List<Long> lList = new ArrayList<>();
        for(Guild g : Main.getJda().getSelfUser().getMutualGuilds()){
            lList.add(g.getIdLong());
        }

        JDACommandValues.DiscordServerPrivacyLink = PrivacyLinkFeatureDataManager.getInstance().load(lList);
    }

    public static long getSlashCommandsRunCount() {
        return slashCommandsRunCount;
    }

    public static void setSlashCommandsRunCount(long slashCommandsRunCount) {
        JDACommandManager.slashCommandsRunCount = slashCommandsRunCount;
    }

    public static long getLastCommandChannelID() {
        return lastCommandChannelID;
    }

    public static CopyOnWriteArrayList<JDACommand> getCommands(){
        return JDA_COMMANDS;
    }

    public static long getLastGuildID() {
        return lastGuildID;
    }
}
