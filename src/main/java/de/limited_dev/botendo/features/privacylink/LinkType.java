package de.limited_dev.botendo.features.privacylink;

public enum LinkType {
    TWITTER("https://twitter.com/"),
    REDDIT("https://www.reddit.com/"),
    YOUTUBE("https://www.youtube.com/watch?v="),
    YOUTUBENOWWW("https://youtube.com/watch?v="),
    YOUBE("https://youtu.be/"),
    NONE("none");
    public final String linkBase;
    LinkType(String linkBase){
        this.linkBase = linkBase;
    }

    public static final LinkType getLinkType(String msgPrt){
        for(LinkType lt : LinkType.values())
            if(msgPrt.startsWith(lt.linkBase))
                return lt;
        return NONE;
    }

}
/*put("youtube.com", "https://piped.kavin.rocks/watch?v=");
 put("reddit.com", "https://teddit.net/");
 put("twitter.com", "https://nitter.net/");*/
