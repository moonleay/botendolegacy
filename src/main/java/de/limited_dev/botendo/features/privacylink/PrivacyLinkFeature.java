package de.limited_dev.botendo.features.privacylink;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.jdacommands.component.JDACommandValues;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.HashMap;
import java.util.Map;

public class PrivacyLinkFeature {
    private static final Map<LinkType, String> websiteToFrontend = new HashMap<>(){{
        put(LinkType.YOUTUBE, "https://piped.kavin.rocks/watch?v=");// www.youtube.com/watch?v= // https://piped.video/watch?v=
        put(LinkType.YOUBE, "https://piped.kavin.rocks/watch?v=");// youtu.be/
        put(LinkType.YOUTUBENOWWW, "https://piped.kavin.rocks/watch?v=");// youtube.com/watch?v=
        put(LinkType.REDDIT, "https://teddit.net/");// www.reddit.com/
        put(LinkType.TWITTER, "https://nitter.net/");// twitter.com/
    }};
    public static void handleInput(MessageReceivedEvent event, String msg){
        if(JDACommandValues.DiscordServerPrivacyLink.get(event.getGuild().getIdLong()) == null || !JDACommandValues.DiscordServerPrivacyLink.get(event.getGuild().getIdLong()))
            return;
        HashMap<String, LinkType> typeList = new HashMap<>();
        if(!msg.contains(" ")){
            LinkType type = LinkType.getLinkType(msg);
            if(type == LinkType.NONE)
                return;
            typeList.put(msg, type);
        }
        else{
            for(String s : msg.split(" ")) {
                LinkType type = LinkType.getLinkType(s);
                if(type == LinkType.NONE)
                    continue;
                typeList.put(s, type);
            }
        }
        StringBuilder replyBody = new StringBuilder();
        for(String s : typeList.keySet()){
            replyBody.append(">>>").append(getReplyMessage(typeList.get(s), s)).append("\n");
        }
        if(typeList.isEmpty())
            return;
        EmbedBuilder eb = EmbeddedMessageHelper.getEmbeddedMessageAutomated(true, "Private Frontend Link", replyBody.toString(),null, true);
        event.getMessage().replyEmbeds(eb.build()).queue();
    }

    public static String getMsgOnly(String url){
        LinkType type = LinkType.getLinkType(url);
        if(type == LinkType.NONE)
            return "";
        return getReplyMessage(type, url);
    }

    public static String getReplyMessage(LinkType type, String msg){
        return websiteToFrontend.get(type) + getLinkTail(type, msg);
    }
    public static String getLinkTail(LinkType type, String msg){
        String returnVal = msg.substring(type.linkBase.length(), msg.length());

        Main.getLgr().info("linkBase: " + type.linkBase + "; msg: " + msg + "; returnval: " + returnVal);
        return returnVal;
    }
}