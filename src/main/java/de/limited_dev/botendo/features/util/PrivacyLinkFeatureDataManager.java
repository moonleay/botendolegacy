package de.limited_dev.botendo.features.util;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

public class PrivacyLinkFeatureDataManager {
    private static PrivacyLinkFeatureDataManager privacyLinkFeatureDataManager;
    private final String basePath = "./data/";
    private final String filename = "PrivacyLinkFeature.nils";
    private final String filePath = basePath + filename;

    public static PrivacyLinkFeatureDataManager getInstance(){
        if(privacyLinkFeatureDataManager == null){
            privacyLinkFeatureDataManager = new PrivacyLinkFeatureDataManager();
        }
        return privacyLinkFeatureDataManager;
    }

    public HashMap<Long, Boolean> load(List<Long> guilds){
        File dir = new File(basePath);
        if(!dir.exists()){
            save(new HashMap<>());
            return new HashMap<>();
        }
        File configFile = new File(dir, filename);
        if(!configFile.exists()){
            save(new HashMap<>());
            return new HashMap<>();
        }
        HashMap<Long, Boolean> hm = new HashMap<>();
        try{
            InputStream input = new FileInputStream(filePath);
            Properties prop = new Properties();

            prop.load(input);
            for(Long l : guilds){
                if(prop.getProperty(l.toString()) == null)
                    continue;
                hm.put(l, Objects.equals(prop.getProperty(l.toString()), "true"));
            }
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hm;
    }

    public void save(HashMap<Long, Boolean> hm){
        File dir = new File(basePath);
        if(!dir.exists()){
            dir.mkdirs();
        }
        File configFile = new File(dir, filename);
        if(!configFile.exists()){
            try{
                configFile.createNewFile();
            } catch(IOException e){
                e.printStackTrace();
            }
        }
        try{
            OutputStream output = new FileOutputStream(filePath);
            Properties prop = new Properties();

            for(Long l : hm.keySet()){
                prop.setProperty(l.toString(), hm.get(l) ? "true" : "false");
            }

            prop.store(output, null);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
