package de.limited_dev.botendo.features.music.component;

import de.limited_dev.botendo.Main;
import net.dv8tion.jda.api.entities.Guild;

import java.util.ArrayList;
import java.util.List;

public class Playlist {
    private final long guildID;
    private final long ownerID;
    private final String playlistName;
    private final boolean _public;

    private final List<String> linkList;

    public Playlist(long guildID, long ownerID, String playlistName, boolean isPublic) {
        this.guildID = guildID;
        this.ownerID = ownerID;
        this.playlistName = playlistName;
        this.linkList = new ArrayList<>();
        this._public = isPublic;
    }

    public boolean addSong(String link) {
        linkList.add(link);
        return true;
    }

    public boolean removeSongWithIndex(int index) {
        linkList.remove(index);
        return true;
    }

    public long getOwnerID() {
        return ownerID;
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public List<String> getLinkList() {
        return linkList;
    }

    public long getGuildID() {
        return guildID;
    }

    public Guild getGuild() {
        return Main.getJda().getGuildById(getGuildID());
    }

    public boolean isPublic() {
        return _public;
    }
}
