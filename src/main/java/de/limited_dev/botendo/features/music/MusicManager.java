package de.limited_dev.botendo.features.music;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import de.limited_dev.botendo.features.music.component.MemeSupplier;
import de.limited_dev.botendo.features.music.component.MemeType;
import de.limited_dev.botendo.util.EmbeddedMessageHelper;
import de.limited_dev.botendo.util.TimeUtil;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.util.HashMap;
import java.util.Map;

public class MusicManager {

    private static MusicManager instance;

    private final AudioPlayerManager playerManager;
    private final Map<Long, GuildMusicManager> musicManagerMap;
    public MusicManager(){
        this.playerManager = new DefaultAudioPlayerManager();
        this.musicManagerMap = new HashMap<>();


        AudioSourceManagers.registerRemoteSources(this.playerManager);
        AudioSourceManagers.registerLocalSource(this.playerManager);
    }

    public GuildMusicManager getGuildMusicManager(Guild guild) {
        return this.musicManagerMap.computeIfAbsent(guild.getIdLong(), (guildId) -> {
            final GuildMusicManager guildMusicManager = new GuildMusicManager(this.playerManager);

            guild.getAudioManager().setSendingHandler(guildMusicManager.getSendHandler());

            return guildMusicManager;
        });
    }

    public void loadAndPlay(SlashCommandInteractionEvent event, String trackUrl) {
        loadAndPlay(event, trackUrl, false);
    }

    public void loadAndPlay(SlashCommandInteractionEvent event, String trackUrl, boolean silent) {
        final GuildMusicManager musicManager = this.getGuildMusicManager(event.getGuild());

        this.playerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler() {
            @Override
            public void trackLoaded(AudioTrack track) {
                musicManager.scheduler.queue(track);
                if (silent)
                    return;
                AudioTrackInfo info = track.getInfo();

                EmbeddedMessageHelper.sendSimpleOneLiner(event, "Added to queue from link",
                        "**" + info.title + "**\n" +
                                "by " + info.author + "\n" +
                                TimeUtil.getTimeFormatedShortend(info.length) + "\n\n" +
                                ">>>" + info.uri, "https://img.youtube.com/vi/" + getImgURL(info.uri) + "/maxresdefault.jpg");
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist) {
                AudioTrack track = playlist.getTracks().get(0);
                if (silent)
                    return;
                AudioTrackInfo info = track.getInfo();

                EmbeddedMessageHelper.sendSimpleOneLiner(event, "Added to queue from query",
                        "**" + info.title + "**\n" +
                                "by " + info.author + "\n" +
                                TimeUtil.getTimeFormatedShortend(info.length) + "\n\n" +
                                ">>>" + info.uri, "https://img.youtube.com/vi/" + getImgURL(info.uri) + "/maxresdefault.jpg");

                musicManager.scheduler.queue(track);
            }

            @Override
            public void noMatches() {
                if (silent)
                    return;
                EmbeddedMessageHelper.sendSimpleOneLiner(event, "No Matches", "No Matches found", MemeSupplier.getMemeLink(MemeType.NO_MATCHES));
            }

            @Override
            public void loadFailed(FriendlyException exception) {
                if (silent)
                    return;
                EmbeddedMessageHelper.sendSimpleOneLiner(event, "Load failed", "Could not load song.", MemeSupplier.getMemeLink(MemeType.LOAD_FAILED));
            }
        });
    }

    private String getImgURL(String uri){
        return uri.split("=")[1];
    }

    public static MusicManager getInstance(){
        if(instance == null){
            instance = new MusicManager();
        }
        return instance;
    }
}
