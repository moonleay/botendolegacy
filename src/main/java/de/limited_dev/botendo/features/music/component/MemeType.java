package de.limited_dev.botendo.features.music.component;

public enum MemeType {
    STOP,
    NO_MATCHES,
    QUEUE,
    EMPTY_QUEUE,
    LOAD_FAILED
}
