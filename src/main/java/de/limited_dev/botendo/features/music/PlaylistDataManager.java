package de.limited_dev.botendo.features.music;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.util.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PlaylistDataManager {
    private final Logger lgr = Main.getLgr();
    private static PlaylistDataManager playlistDataManager;
    private final String basePath = "./data/playlists/";

    private PlaylistDataManager() {

    }

    public static PlaylistDataManager getInstance() {
        if (playlistDataManager == null) {
            playlistDataManager = new PlaylistDataManager();
        }
        return playlistDataManager;
    }

    public List<Playlist> loadGuild(long guildID) {
        File dir = new File(basePath + guildID + "/");
        if (!dir.exists()) {
            return null;
        }
        List<Playlist> playlists = new ArrayList<>();
        for (File d : dir.listFiles()) {
            if (!d.exists()) {
                lgr.info("File (" + d.getName() + ") does not exist, this should not happen");
                continue;
            }
            try {
                InputStream input = new FileInputStream(d.getAbsolutePath() + "/playlist.metadata");
                Properties prop = new Properties();
                prop.load(input);

                long ownerid = Long.parseLong(prop.getProperty("ownerid"));
                long guildid = Long.parseLong(prop.getProperty("guidid"));
                String name = prop.getProperty("name");
                boolean isPublic = prop.getProperty("ispublic").contains("true");
                input.close();

                lgr.info("Loading Playlist " + name);
                Playlist pl = new Playlist(guildid, ownerid, name, isPublic);

                if (Main.getJda().getGuildById(guildid).getMemberById(ownerid) == null) {
                    removePlaylist(pl);
                    continue;
                }


                FileReader reader = new FileReader(d.getAbsolutePath() + "/playlist.list");
                BufferedReader bufferedReader = new BufferedReader(reader);

                String line;

                while ((line = bufferedReader.readLine()) != null && line != "") {
                    pl.addSong(line);
                }

                playlists.add(pl);
                bufferedReader.close();
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return playlists;
    }

    public void removePlaylist(Playlist list) {
        String filePath = basePath + "/" + list.getGuildID() + "/" + list.getPlaylistName();
        File playlistFolder = new File(filePath);
        if (playlistFolder.exists()) {
            lgr.info("Deleted " + playlistFolder.getName() + " sucessfully");
            deleteFile(playlistFolder);
            return;
        }
        lgr.info("The folder " + playlistFolder.getName() + " does not exist.");
    }

    public void savePlaylist(Playlist list) {
        File dir = new File(basePath + "/" + list.getGuildID() + "/" + list.getPlaylistName());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        savePlaylistMeta(list);
        String filePath = basePath + "/" + list.getGuildID() + "/" + list.getPlaylistName() + "/playlist.list";
        File configFile = new File(filePath);
        if (configFile.exists()) {
            configFile.delete();
        }
        try {
            configFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileWriter writer = new FileWriter(configFile.getAbsolutePath(), true);
            for (String s : list.getLinkList()) {
                writer.write(s);
                writer.write("\r\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void savePlaylistMeta(Playlist list) {
        String filePath = basePath + "/" + list.getGuildID() + "/" + list.getPlaylistName() + "/playlist.metadata";
        File f = new File(basePath);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        try {
            OutputStream output = new FileOutputStream(filePath);
            Properties prop = new Properties();

            prop.setProperty("ownerid", list.getOwnerID() + "");
            prop.setProperty("guidid", list.getGuildID() + "");
            prop.setProperty("name", list.getPlaylistName());
            prop.setProperty("ispublic", list.isPublic() ? "true" : "false");

            prop.store(output, null);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteFile(File element) {
        if (element.isDirectory()) {
            for (File sub : element.listFiles()) {
                deleteFile(sub);
            }
        }
        element.delete();
    }
}
