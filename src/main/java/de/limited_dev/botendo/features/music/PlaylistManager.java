package de.limited_dev.botendo.features.music;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.features.music.component.Playlist;
import de.limited_dev.botendo.util.Logger;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class PlaylistManager {
    private static final PlaylistDataManager playlistDataManager = Main.getPlaylistDataManager();
    private static final Logger lgr = Main.getLgr();
    public static HashMap<Long, List<Playlist>> guildPlaylistMap = new HashMap<>();

    public static void loadAllPlaylists() {
        int index = 0;
        for (Guild g : Main.getJda().getSelfUser().getMutualGuilds()) {
            lgr.info("Loading Playlists from Guild: " + g.getName());
            guildPlaylistMap.put(g.getIdLong(), playlistDataManager.loadGuild(g.getIdLong()) == null ? new ArrayList<Playlist>() : Main.getPlaylistDataManager().loadGuild(g.getIdLong()));
            index += guildPlaylistMap.get(g.getIdLong()) == null ? 0 : guildPlaylistMap.get(g.getIdLong()).size();
        }
        lgr.info("Loaded " + index + " playlists");
    }

    public static List<Playlist> getListOfPlaylistsForGuild(Guild g) {
        return guildPlaylistMap.get(g.getIdLong());
    }

    public static Playlist getPlaylistFromName(Guild g, String name) {
        for (Playlist pl : guildPlaylistMap.get(g.getIdLong())) {
            if (Objects.equals(pl.getPlaylistName(), name)) {
                return pl;
            }
        }
        return null;
    }

    public static boolean addPlaylist(Playlist pl) {
        if (getPlaylistFromName(pl.getGuild(), pl.getPlaylistName()) != null)
            return false;

        guildPlaylistMap.get(pl.getGuildID()).add(pl);
        playlistDataManager.savePlaylist(pl);
        return true;
    }

    public static boolean removePlaylist(Playlist pl, User u) {
        for (Playlist playlist : guildPlaylistMap.get(pl.getGuildID())) {
            if (Objects.equals(playlist.getPlaylistName(), pl.getPlaylistName()) && pl.getOwnerID() == u.getIdLong()) {
                guildPlaylistMap.get(pl.getGuildID()).remove(playlist);
                playlistDataManager.removePlaylist(pl);
                return true;
            }
        }
        return false;
    }
}
