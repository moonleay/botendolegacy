package de.limited_dev.botendo.features.exceptionhandler;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.jdacommands.component.JDACommandManager;
import de.limited_dev.botendo.util.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class BotExceptionHandler implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Logger lgr = Main.getLgr();
        lgr.info("Uncaught exception");
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String stacktrace = sw.toString();
        sw.flush();
        pw.flush();
        try {
            sw.close();
            pw.close();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        if (JDACommandManager.getLastCommandChannelID() == 0L) {
            lgr.info("No message sent yet");
            return;
        }
        Main.getJda().getGuildById(JDACommandManager.getLastCommandChannelID()).getMemberById(372703841151614976L).getUser().openPrivateChannel().submit()
                .thenCompose(channel -> channel.sendMessage("The bot had an uncaught exception!\nStacktrace:\n" + stacktrace).submit())
                .whenComplete((message, error) -> {
                });
    }
}
