package de.limited_dev.botendo.util;

import de.limited_dev.botendo.Main;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class TimeUtil {

    public static String getTimeFormatedShortend(long time) {
        long days = TimeUnit.MILLISECONDS
                .toDays(time);
        time -= TimeUnit.DAYS.toMillis(days);

        long hours = TimeUnit.MILLISECONDS
                .toHours(time);
        time -= TimeUnit.HOURS.toMillis(hours);

        long minutes = TimeUnit.MILLISECONDS
                .toMinutes(time);
        time -= TimeUnit.MINUTES.toMillis(minutes);

        long seconds = TimeUnit.MILLISECONDS
                .toSeconds(time);
        String s = "";
        if(days >= 1){
            s += days + "d ";
        }
        if (hours >= 1) {
            s += hours + "h ";
        }
        if (minutes >= 1) {
            s += minutes + "m ";
        }
        if (seconds >= 1 && hours < 1) {
            s += seconds + "s";
        }

        if (s.isEmpty() || s.isBlank()) {
            s = "0s";
        }
        return s;
    }

    public static String getTimeFormatedRaw(long time) {
        long days = TimeUnit.MILLISECONDS
                .toDays(time);
        time -= TimeUnit.DAYS.toMillis(days);

        long hours = TimeUnit.MILLISECONDS
                .toHours(time);
        time -= TimeUnit.HOURS.toMillis(hours);

        long minutes = TimeUnit.MILLISECONDS
                .toMinutes(time);
        time -= TimeUnit.MINUTES.toMillis(minutes);

        long seconds = TimeUnit.MILLISECONDS
                .toSeconds(time);
        String s = "";
        if (days >= 1) {
            s += days + "d ";
        }
        if (hours >= 1) {
            s += hours + "h ";
        }
        if (minutes >= 1) {
            s += minutes + "m ";
        }
        if (seconds >= 1) {
            s += seconds + "s";
        }

        if (s.isEmpty() || s.isBlank()) {
            s = "0s";
        }
        return s;
    }


    //This 100000%ly can be improved, I wrote this at 2am
    public static long getTimeUnformated(String timeStr) {
        long days = 0, hours = 0, minutes = 0, seconds = 0;
        String[] timeArr = timeStr.split(" ");
        for (String s : timeArr) {
            Main.getLgr().info(s);
            if (s.endsWith("d")) {
                days = Long.parseLong(s.split("d")[0]);
            } else if (s.endsWith("h")) {
                hours = Long.parseLong(s.split("h")[0]);
            } else if (s.endsWith("m")) {
                minutes = Long.parseLong(s.split("m")[0]);
            } else if (s.endsWith("s")) {
                seconds = Long.parseLong(s.split("s")[0]);
            }
        }
        Main.getLgr().info(Duration.ofSeconds(seconds).plus(Duration.ofMinutes(minutes)).plus(Duration.ofHours(hours)).plus(Duration.ofDays(days)).toMillis() + "");
        return Duration.ofSeconds(seconds).plus(Duration.ofMinutes(minutes)).plus(Duration.ofHours(hours)).plus(Duration.ofDays(days)).toMillis();
    }
}
