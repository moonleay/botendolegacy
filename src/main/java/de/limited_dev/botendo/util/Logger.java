package de.limited_dev.botendo.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm:ss");

    public void info(String msg) {
        StackTraceElement caller = Thread.currentThread().getStackTrace()[2];
        LocalDateTime now = LocalDateTime.now();
        try {
            System.out.println("[" + Class.forName(caller.getClassName()).getSimpleName() + "." +
                    caller.getMethodName() + ":" + caller.getLineNumber() + "] [" + dtf.format(now) + "] <" + msg + ">");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        // Ich kann nicht mehr
        // [Klasse.Funktion] [T/M HH:MM] <NACHRICHT>
    }

    public void consoleOut(String msg) {
        LocalDateTime now = LocalDateTime.now();
        System.out.println("[console command output] [" + dtf.format(now) + "] <" + msg + ">");
    }
}
