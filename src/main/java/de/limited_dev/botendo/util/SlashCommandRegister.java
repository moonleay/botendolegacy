package de.limited_dev.botendo.util;

import de.limited_dev.botendo.Main;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class SlashCommandRegister {

    public static void registerCommands(){
        Main.getJda().updateCommands().addCommands(
                Commands.slash("info", "Shows Info about the bot")
                        .addOptions(new OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices")),
                Commands.slash("help", "Show a list of all bot commands")
                        .addOptions(new OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices")),
                Commands.slash("userinfo", "Shows info about users")
                        .addOptions(new OptionData(OptionType.USER, "target", "Target User", true))
                        .addOptions(new OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices")),
                Commands.slash("timeformatter", "Get the timestamp of provided numbers")
                        .addOptions(new OptionData(OptionType.STRING, "unixtime", "Timestamp as long", true)),
                Commands.slash("timeunformatter", "Get the unixtime from a Timestamp")
                        .addOptions(new OptionData(OptionType.STRING, "timestamp", "Timestamp as String", true)),
                Commands.slash("buttonroles", "Add Buttons to gain roles when you react to them")
                        .addOptions(new OptionData(OptionType.ROLE, "role1", "1st Role", true),
                                new OptionData(OptionType.ROLE, "role2", "2nd Role"),
                                new OptionData(OptionType.ROLE, "role3", "3rd Role"),
                                new OptionData(OptionType.ROLE, "role4", "4th Role"),
                                new OptionData(OptionType.ROLE, "role5", "5th Role")),
                Commands.slash("play", "Play music with the bot")
                        .addOptions(new OptionData(OptionType.STRING, "linkquery", "YouTube Link / Search Query", true)),
                Commands.slash("stop", "Stop the music & Bot leaves the voice channel"),
                Commands.slash("nowplaying", "Show what's currently playing"),
                Commands.slash("queue", "Show the music queue"),
                Commands.slash("skip", "Skip the current song"),
                Commands.slash("pause", "Pause the current song"),
                Commands.slash("continue", "Continue the current song, when paused"),
                Commands.slash("repeat", "Toggle repeating the current song"),
                Commands.slash("addplaylist", "Add a playlist")
                        .addOptions(new OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true))
                        .addOptions(new OptionData(OptionType.BOOLEAN, "public", "Is this a public or private playlist", true)),
                Commands.slash("listplaylists", "List all playlists")
                        .addOptions(new OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices")),
                Commands.slash("removeplaylist", "Remove a playlist")
                        .addOptions(new OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true)),
                Commands.slash("addsongtoplaylist", "Add a Song to a playlist")
                        .addOptions(new OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true))
                        .addOptions(new OptionData(OptionType.STRING, "youtubelink", "YouTube Link to the desired song", true)),
                Commands.slash("removesongfromplaylist", "Add a Song to a playlist")
                        .addOptions(new OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true))
                        .addOptions(new OptionData(OptionType.INTEGER, "index", "Index of wanted song", true)),
                Commands.slash("listallinplaylist", "List all songs in a playlist")
                        .addOptions(new OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true)),
                Commands.slash("playplaylist", "Play a playlist")
                        .addOptions(new OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true)),
                Commands.slash("exportplaylist", "Export a playlist")
                        .addOptions(new OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true)),
                Commands.slash("importplaylist", "Export a playlist")
                        .addOptions(new OptionData(OptionType.ATTACHMENT, "file", "The database data", true))
                        .addOptions(new OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true))
                        .addOptions(new OptionData(OptionType.BOOLEAN, "public", "Is this a public or private playlist", true)),
                Commands.slash("poll", "Create a pol")
                        .addOptions(new OptionData(OptionType.STRING, "caption", "Caption the poll", true))
                        .addOptions(new OptionData(OptionType.STRING, "option1", "The 1st option", true))
                        .addOptions(new OptionData(OptionType.STRING, "option2", "The 2nd option", true))
                        .addOptions(new OptionData(OptionType.STRING, "option3", "The 3rd option"))
                        .addOptions(new OptionData(OptionType.STRING, "option4", "The 4th option"))
                        .addOptions(new OptionData(OptionType.STRING, "option5", "The 5th option"))
                        .addOptions(new OptionData(OptionType.STRING, "option6", "The 6th option"))
                        .addOptions(new OptionData(OptionType.STRING, "option7", "The 7th option"))
                        .addOptions(new OptionData(OptionType.STRING, "option8", "The 8th option"))
                        .addOptions(new OptionData(OptionType.STRING, "option9", "The 9th option"))
                        .addOptions(new OptionData(OptionType.STRING, "option10", "The 10th option")),
                Commands.slash("feature", "Change a feature")
                        .addOptions(new OptionData(OptionType.STRING, "type", "Do you want to add or remove a feature", true)
                                .addChoice("Add", "add")
                                .addChoice("Remove", "remove"))
                        .addOptions(new OptionData(OptionType.STRING, "feature", "The name of the feature", true)
                                .addChoice("Privacy Link Replier", "privacylinkreplier"))
        ).queue();

    }
}
