package de.limited_dev.botendo.consolecommands.component;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.util.Logger;
import net.dv8tion.jda.api.JDA;

public class ConsoleCommand {
    private final String name;
    private final String description;
    private final String[] args;
    protected final Logger lgr = Main.getLgr();
    protected final JDA jda = Main.getJda();

    public ConsoleCommand(String name, String description, String[] args){
        this.name = name;
        this.description = description;
        this.args = args;
    }

    public void onCommand(String[] args){

    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String[] getArgs() {
        return args;
    }
}
