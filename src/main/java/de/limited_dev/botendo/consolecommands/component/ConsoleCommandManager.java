package de.limited_dev.botendo.consolecommands.component;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.consolecommands.system.BotVersionCommand;
import de.limited_dev.botendo.consolecommands.system.ShutdownCommand;
import de.limited_dev.botendo.consolecommands.util.HelpConsoleCommand;
import de.limited_dev.botendo.consolecommands.util.ManualCommand;
import de.limited_dev.botendo.util.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class ConsoleCommandManager {
    private static List<ConsoleCommand> commands = new ArrayList<>();
    private static final Logger lgr = Main.getLgr();

    public static void registerCommands(){
        commands.add(new ShutdownCommand());
        commands.add(new HelpConsoleCommand());
        commands.add(new ManualCommand());
        commands.add(new BotVersionCommand());
    }

    public static void registerListener(){
        Thread thr = new Thread(){
            @Override
            public void run() {
                while(true){
                    if(handleConsoleInput()){
                        break;
                    }
                }
            }
        };
        thr.start();
    }

    public static boolean handleConsoleInput(){
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        lgr.info("Console input: " + input);
        String[] commandArr = getCommandArray(input);
        String command = commandArr[0];
        String[] args = getArgumentsFromCommandArray(commandArr);
        for(ConsoleCommand c : commands){
            if(Objects.equals(c.getName(), command)){
                c.onCommand(args);
            }
        }
        if(Objects.equals("shutdown", command)){
            return true;
        }
        return false;
    }

    private static String[] getCommandArray(String command) {
        return command.split(" ", 0);
    }

    private static String[] getArgumentsFromCommandArray(String[] commandArr) {
        String[] args = new String[commandArr.length - 1];
        for (int i = 1, j = 0; i < commandArr.length; i++) {
            args[j++] = commandArr[i];
        }
        return args;
    }

    public static ConsoleCommand getCommand(String name){
        for(ConsoleCommand c : commands){
            if(Objects.equals(c.getName(), name))
                return c;
        }
        return null;
    }

    public static List<ConsoleCommand> getCommands() {
        return commands;
    }
}
