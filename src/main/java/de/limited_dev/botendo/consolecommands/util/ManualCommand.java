package de.limited_dev.botendo.consolecommands.util;

import de.limited_dev.botendo.consolecommands.component.ConsoleCommand;
import de.limited_dev.botendo.consolecommands.component.ConsoleCommandManager;

public class ManualCommand extends ConsoleCommand {
    public ManualCommand() {
        super("man", "Show the correct usage of a command & its description", new String[]{"Command Name"});
    }

    @Override
    public void onCommand(String[] args) {
        if(args.length != 1){
            sendUsage(this.getName());
            return;
        }
        String commandName = args[0];
        sendUsage(commandName.toLowerCase());
    }

    private void sendUsage(String commandName){
        ConsoleCommand command = ConsoleCommandManager.getCommand(commandName);
        if(command == null){
            sendUsage(this.getName());
            return;
        }
        String out = "Command: " + command.getName() +  " = " + command.getDescription() + "";
        lgr.consoleOut(out);

        out = "Usage: " + commandName;
        if(command.getArgs() == null){
            lgr.consoleOut(out);
            return;
        }
        out = out + (command.getArgs().length == 0 ? "" : " ");
        for(int i = 0; i < command.getArgs().length; ++i){
            out = out + "<" + command.getArgs()[i] + ">" + (command.getArgs().length - 1 == i ? "" : " ");
        }
        lgr.consoleOut(out);
    }
}
