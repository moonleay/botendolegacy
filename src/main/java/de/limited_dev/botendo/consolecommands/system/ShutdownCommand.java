package de.limited_dev.botendo.consolecommands.system;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.consolecommands.component.ConsoleCommand;
import de.limited_dev.botendo.util.StatusManager;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.managers.AudioManager;

public class ShutdownCommand extends ConsoleCommand {
    public ShutdownCommand() {
        super("shutdown", "Shutdown the Bot", null);
    }

    @Override
    public void onCommand(String[] args) {
        lgr.consoleOut("Shutting down...");
        for(Guild g : jda.getSelfUser().getMutualGuilds()){
            Member self = g.getSelfMember();
            GuildVoiceState selfState = self.getVoiceState();
            if(selfState.inAudioChannel()){
                AudioManager audioManager = g.getAudioManager();
                lgr.consoleOut("Leaving VC " + audioManager.getConnectedChannel().asVoiceChannel().getName() + " in " + g.getName());
                audioManager.closeAudioConnection();
            }
        }
        StatusManager.schedulerService.shutdown();
        jda.shutdown();
    }
}
