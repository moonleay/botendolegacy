package de.limited_dev.botendo.consolecommands.system;

import de.limited_dev.botendo.build.BuildConstants;
import de.limited_dev.botendo.consolecommands.component.ConsoleCommand;

public class BotVersionCommand extends ConsoleCommand {
    public BotVersionCommand() {
        super("version", "Displays the Bot version", null);
    }

    @Override
    public void onCommand(String[] args) {
        lgr.consoleOut(jda.getSelfUser().getName() + " on " + BuildConstants.botVersion);
    }
}
