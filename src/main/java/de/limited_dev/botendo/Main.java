package de.limited_dev.botendo;

import de.limited_dev.botendo.build.BuildConstants;
import de.limited_dev.botendo.consolecommands.component.ConsoleCommandManager;
import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.jdacommands.component.JDACommandManager;
import de.limited_dev.botendo.listeners.*;
import de.limited_dev.botendo.util.Logger;
import de.limited_dev.botendo.features.music.PlaylistDataManager;
import de.limited_dev.botendo.util.StatusManager;
import de.limited_dev.botendo.util.TokenManager;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;

public class Main {

    private static JDA jda;
    private static final Logger lgr = new Logger();
    private static long launchTime;
    private static final TokenManager tokenManager = TokenManager.getInstance();
    private static final PlaylistDataManager playlistDataManager = PlaylistDataManager.getInstance();
    public static void main(String[] args) throws LoginException {
        System.out.println(" ___       _                  _      \n" +
                "| _ ) ___ | |_  ___  _ _   __| | ___ \n" +
                "| _ \\/ _ \\|  _|/ -_)| ' \\ / _` |/ _ \\\n" +
                "|___/\\___/ \\__|\\___||_||_|\\__/_|\\___/\n");
        lgr.info("Loading Discord Bot ...");
        lgr.info(BuildConstants.botVersion);


        launchTime = System.currentTimeMillis();

        tokenManager.load();

        //storageManager.load();

        jda = JDABuilder.createDefault(tokenManager.getToken())
                .setActivity(Activity.playing("mit Huebina")) //mit Huebina
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .enableIntents(GatewayIntent.GUILD_PRESENCES)
                .enableIntents(GatewayIntent.GUILD_MEMBERS)
                .enableIntents(GatewayIntent.MESSAGE_CONTENT)
                .enableIntents(GatewayIntent.GUILD_VOICE_STATES)
                .enableCache(CacheFlag.CLIENT_STATUS)
                .setMemberCachePolicy(MemberCachePolicy.ALL)
                .build();

        jda.addEventListener(new ReadyListener());
        jda.addEventListener(new VoiceChannelListener());
        jda.addEventListener(new ChatLinkListener());
        jda.addEventListener(new ButtonInteractionListener());
        jda.addEventListener(new SlashCommandInteractionListener());
        jda.addEventListener(new GuildMemberListener());

        try {
            jda.awaitReady();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        if (jda.getSelfUser().getName().contains("Test")) {
            lgr.info("Test mode active.");
            jda.getPresence().setStatus(OnlineStatus.IDLE);
        }

        StatusManager.startThread();

        lgr.info("Logged into: " + jda.getSelfUser().getName());


        JDACommandManager.registerCommands();
        ConsoleCommandManager.registerCommands();
        ConsoleCommandManager.registerListener();
        PlaylistManager.loadAllPlaylists();


        //Thread.setDefaultUncaughtExceptionHandler(new BotExceptionHandler());



        lgr.info("Done.");
    }

    public static JDA getJda() {
        return jda;
    }

    public static Logger getLgr() {
        return lgr;
    }

    public static long getUptime() {
        return System.currentTimeMillis() - launchTime;
    }

    public static PlaylistDataManager getPlaylistDataManager() {
        return playlistDataManager;
    }
}
