package de.limited_dev.botendo.listeners;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.jdacommands.component.JDACommand;
import de.limited_dev.botendo.jdacommands.component.JDACommandManager;
import de.limited_dev.botendo.util.Logger;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class SlashCommandInteractionListener extends ListenerAdapter {

    private Logger lgr = Main.getLgr();
    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
        lgr.info("New Command: /" + event.getName() + " in " + event.getGuild().getName());
        for(JDACommand c : JDACommandManager.getCommands()){
            if(event.getName().equals(c.getName())){
                c.onSlashCommand(event);
                JDACommandManager.setSlashCommandsRunCount(JDACommandManager.getSlashCommandsRunCount() + 1);
            }
        }
    }
}
