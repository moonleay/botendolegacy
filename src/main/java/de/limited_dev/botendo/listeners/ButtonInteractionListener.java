package de.limited_dev.botendo.listeners;

import de.limited_dev.botendo.Main;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class ButtonInteractionListener extends ListenerAdapter {
    @Override
    public void onButtonInteraction(@NotNull ButtonInteractionEvent event) {
        Guild g = event.getGuild();
        if(event.getMessage().getAuthor().getIdLong() != event.getJDA().getSelfUser().getIdLong()){
            event.deferEdit().queue();
            return;
        }
        Role r = g.getRoleById(event.getButton().getId());
        if(r == null)
            return;
        Member m = event.getInteraction().getMember();
        Main.getLgr().info("Member: " + m.getUser().getName() + " Role: " + r.getName());
        if(m.getRoles().contains(r)){
            g.removeRoleFromMember(m, r).queue();
            event.deferEdit().queue();
            return;
        }
        g.addRoleToMember(m, r).queue();
        event.deferEdit().queue();
    }
}
