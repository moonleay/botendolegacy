package de.limited_dev.botendo.listeners;

import de.limited_dev.botendo.features.privacylink.PrivacyLinkFeature;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class ChatLinkListener extends ListenerAdapter {

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        if(event.getAuthor().isSystem())
            return;
        String msg = event.getMessage().getContentDisplay();
        if(!msg.contains("http"))
            return;
        PrivacyLinkFeature.handleInput(event, msg);
    }
}