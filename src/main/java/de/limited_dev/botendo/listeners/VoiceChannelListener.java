package de.limited_dev.botendo.listeners;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.features.music.GuildMusicManager;
import de.limited_dev.botendo.features.music.MusicManager;
import de.limited_dev.botendo.util.Logger;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.managers.AudioManager;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class VoiceChannelListener extends ListenerAdapter {

    private static final Logger lgr = Main.getLgr();

    @Override
    public void onGuildVoiceJoin(@NotNull GuildVoiceJoinEvent event) {
        Member m = event.getMember();
        Guild g = event.getGuild();
        lgr.info("User " + m.getUser().getName() + " joined in " + g.getName() + "!");
    }

    @Override
    public void onGuildVoiceLeave(@NotNull GuildVoiceLeaveEvent event) {
        Member m = event.getMember();
        Guild g = event.getGuild();
        lgr.info("User " + m.getUser().getName() + " left in " + g.getName() + "!");
        if(Objects.equals(m, g.getSelfMember())){
            final GuildMusicManager guildMusicManager = MusicManager.getInstance().getGuildMusicManager(g);
            guildMusicManager.scheduler.player.stopTrack();
            guildMusicManager.scheduler.queue.clear();
            final AudioManager audioManager = g.getAudioManager();
            audioManager.closeAudioConnection();
            lgr.info("quit selfmember");
        }

        if(g.getSelfMember().getVoiceState().inAudioChannel() && g.getSelfMember().getVoiceState().getChannel().asVoiceChannel().getMembers().size() == 1){
            final GuildMusicManager guildMusicManager = MusicManager.getInstance().getGuildMusicManager(g);
            guildMusicManager.scheduler.player.stopTrack();
            guildMusicManager.scheduler.queue.clear();
            final AudioManager audioManager = g.getAudioManager();
            audioManager.closeAudioConnection();
            lgr.info("quit selfmember");
        }
    }

    @Override
    public void onGuildVoiceMove(@NotNull GuildVoiceMoveEvent event) {
        Member m = event.getMember();
        Guild g = event.getGuild();
        lgr.info("User " + m.getUser().getName() + " moved in " + g.getName() + "!");
        if(Objects.equals(m, g.getSelfMember())){
            final GuildMusicManager guildMusicManager = MusicManager.getInstance().getGuildMusicManager(g);
            guildMusicManager.scheduler.player.stopTrack();
            guildMusicManager.scheduler.queue.clear();
            final AudioManager audioManager = g.getAudioManager();
            audioManager.closeAudioConnection();
            lgr.info("quit selfmember");
        }

        if(g.getSelfMember().getVoiceState().inAudioChannel() && g.getSelfMember().getVoiceState().getChannel().asVoiceChannel().getMembers().size() == 1){
            final GuildMusicManager guildMusicManager = MusicManager.getInstance().getGuildMusicManager(g);
            guildMusicManager.scheduler.player.stopTrack();
            guildMusicManager.scheduler.queue.clear();
            final AudioManager audioManager = g.getAudioManager();
            audioManager.closeAudioConnection();
            lgr.info("quit selfmember");
        }
    }
}
