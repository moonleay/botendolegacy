package de.limited_dev.botendo.listeners;

import de.limited_dev.botendo.Main;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class GuildListener extends ListenerAdapter {
    @Override
    public void onGuildJoin(@NotNull GuildJoinEvent event) {
        Guild g = event.getGuild();
        for(Member m : g.getMembers()){
            if(m.getVoiceState().inAudioChannel()){
                Main.getLgr().info("User " + m.getUser().getName() + " is in VC!");
            }
        }
    }
}
