package de.limited_dev.botendo.listeners;

import de.limited_dev.botendo.Main;
import de.limited_dev.botendo.util.Logger;
import de.limited_dev.botendo.util.SlashCommandRegister;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class ReadyListener extends ListenerAdapter {
    private final Logger lgr = Main.getLgr();

    @Override
    public void onReady(@NotNull ReadyEvent event) {
        SlashCommandRegister.registerCommands();
        lgr.info("Client ready");
    }
}
