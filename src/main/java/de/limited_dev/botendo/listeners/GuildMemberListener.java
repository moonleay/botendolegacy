package de.limited_dev.botendo.listeners;

import de.limited_dev.botendo.features.music.PlaylistManager;
import de.limited_dev.botendo.features.music.component.Playlist;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class GuildMemberListener extends ListenerAdapter {
    @Override
    public void onGuildMemberRemove(@NotNull GuildMemberRemoveEvent event) {
        Guild g = event.getGuild();
        Member m = event.getMember();
        for (Playlist pl : PlaylistManager.getListOfPlaylistsForGuild(g)) {
            if (pl.getOwnerID() == m.getUser().getIdLong()) {
                PlaylistManager.removePlaylist(pl, m.getUser());
            }
        }
    }
}
